import os
from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, send_file,json,jsonify
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug.urls import url_parse
from app import app, db
from app.forms import LoginForm, RegistrationForm, NewBookForm, SearchForm, EditTitleForm
from app.models import User, Book, Category
from werkzeug import secure_filename
from flask import g
from app.task import create_file_in_background

@app.before_request
def before_request():
    g.search_form = SearchForm()



@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    category_list=Category.query.all()
    return render_template('index.html',category_list=category_list)


@app.route('/newbook',methods=['GET','POST'])
@login_required
def newbook():
    form=NewBookForm()
    if request.method=='POST':
        file = request.files['file']
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
        categories=form.category.data.split(',')
        book = Book(title=form.title.data,summary=form.summary.data,filename=filename,user_id=current_user.id,uploadby=current_user)
        db.session.add(book)
        db.session.commit()
        for c in categories:
            list_of_categories=Category.query.filter_by(name=c).first()
            if(list_of_categories):
                pass
            else:
                cat=Category(name=c)
                db.session.add(cat)
        for c in categories:
            cat=Category.query.filter_by(name=c).first()
            book.book_category.append(cat)
            db.session.commit()
        flash(book.title+' is added to Library')
        return redirect(url_for('newbook'))
    return render_template('new_book.html',form=form)

@app.route('/update/<book_id>',methods=['GET','POST'])
@login_required
def update(book_id):
    form=EditTitleForm()
    book=Book.query.filter_by(id=book_id).first()
    if request.method=="POST":

        book.title=form.new_title.data
        db.session.commit()
        return redirect(url_for('view_book',file_id=book.id))
    return render_template('edit_title.html',form=form,book=book)


@app.route('/view_book/<file_id>')
def view_book(file_id):
    book=Book.query.filter_by(id=file_id).first()
    # with open(os.path.join(app.config['UPLOAD_FOLDER'],book.filename), "r") as f:
        # content = f.read()
    return render_template('view_book.html',book=book)


@app.route('/book/<cat>')
def book(cat):
    books=Category.query.filter_by(id=cat).first().book_cat
    return render_template('book_list.html',books=books)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('user',username=user.username))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('user',username=user.username))
    return render_template('register.html', title='Register', form=form)

@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    uploads=Book.query.filter_by(user_id=current_user.id).all()
    return render_template('profile.html',user=user,uploads=uploads)

@app.route('/download/<filename>')
def download(filename):
    return send_file(('../uploads/'+filename), as_attachment=True,mimetype = 'text/pdf')

@app.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('index'))
    posts, total = Book.search(g.search_form.q.data)
    return render_template('search.html', title=('Search'), posts=posts,total=total)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/download_content',methods=['GET', 'POST'])
def download_content():
    task = create_file_in_background.apply_async()
 
    return jsonify({}) ,{'Location': url_for('check_status',task_id=task.id)}
    

@app.route('/status/<task_id>')
def check_status(task_id):
    task = create_file_in_background.AsyncResult(task_id)
    response = {
        'state': task.state,
    }
    return jsonify(response)

    
@app.route('/send_json_file',methods=['GET'])
def send_json_file():
    return send_file('../a.json', as_attachment=True)